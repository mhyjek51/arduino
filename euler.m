function euler
% A second order differential equation
Tsim = 1;                                            % simulate over Tsim seconds
dt = 0.01;                                           % step size
N= Tsim/dt;                                          % number of steps to simulate
x=zeros(4,N);
t = zeros(1,N);
%--------------------------------------------------------------%|
b = 0.4;                                                       %|
kappa = 1;                                                     %|
M = 1;                                                         %|
g = .01;                                                       %|
Gamma = 0.2;                                                   %|
% Shape function               % F = kappa*x*(1-x);            %|
h0 = kappa*sin(pi*b);          % h0 = F(b);                    %|
theta0 = kappa*pi*cos(pi*b);   % htheta0 = F'(b);              %|
%--------------------------------------------------------------%|

% x is the solution vector x(1,:) = h, x(2,:) = h',
% x(3,:) = theta, x(4,:) = theta'
% For h
x(1,1)= h0;        % h(t = 0) ;
x(2,1)= 0;         % dh/dt (t = 0) = h1;
% For theta
x(3,1)= theta0;    % theta(t = 0);
x(4,1)= 0;         % dtheta/dt(t = 0) = theta1;
for k=1:N-1
    % For time t
    t(k+1)= t(k) + dt;
    xnew=x(:,k)+ dt *MassBalance(t(k),x(:,k));
    x(:,k+1) = x(:,k) + dt/2* (MassBalance(t(k),x(:,k)) + MassBalance(t(k),xnew)); %Modified Euler algorithm
end
%Compare with ode45
x0 = [h0; 0; theta0; 0];
[T,Y] = ode45(@MassBalance,[0 Tsim],x0);   % where Y is the solution vector
[aa, bb]= RK4(h0, theta0, 0, 0, Tsim, N);
end

function dx= MassBalance(t,w)
% use a structure to store the parameters
%--------------------------------------------------------------%|
b = 0.4;                                                       %|
kappa = 1;                                                     %|
M = 1;                                                         %|
g = .01;                                                       %|
Gamma = 0.2;                                                   %|
% Shape function               % F = kappa*x*(1-x);            %|
h0 = kappa*sin(pi*b);          % h0 = F(b);                    %|
theta0 = kappa*pi*cos(pi*b);   % htheta0 = F'(b);              %|
%--------------------------------------------------------------%|
% Equations of motion for the body
dx = zeros(4,1);

xx = (0:0.003:1);

int_func1 = 1/2*(1 - ((w(1)+ (1-b)*w(3))./(w(1)+(xx-b).*w(3)-kappa.*sin(pi.*xx))).^2);
int_func2 = (xx-b).*(1/2*(1 - ((w(1)+ (1-b)*w(3))./(w(1)+(xx-b).*w(3)-kappa.*sin(pi.*xx))).^2));

% plot(xx, integral_func)
I1 = simpsons(int_func1,0,1,[]);
I2 = simpsons(int_func2,0,1,[]);
dx(1)=w(2);
dx(2)=I1 - M*g
dx(3)=w(4);
dx(4)= 1/Gamma.* I2;
end

function [x y u w]= RK4(a0, b0, c0, d0,  t, n)
% x = h; y = theta; u = h'; w = theta';
%--------------------------------------------------------------%|
b = 0.4;                                                       %|
kappa = 1;                                                     %|
M = 1;                                                         %|
g = .01;                                                       %|
Gamma = 0.2;                                                   %|
% Shape function               % F = kappa*x*(1-x);            %|
h0 = kappa*sin(pi*b);          % h0 = F(b);                    %|
theta0 = kappa*pi*cos(pi*b);   % htheta0 = F'(b);              %|
%--------------------------------------------------------------%|





end